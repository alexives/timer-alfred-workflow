# Timer Workflow for Alfred 4

I wanted a simple timer I could use from within alfred.

## Installation

- [Download the latest artifact](https://gitlab.com/alexives/timer-alfred-workflow/-/jobs/artifacts/master/download?job=build&file=Timer.alfredworkflow)
- Open the alfredworkflow file in Alfred

## Usage

From alfred, type `timer <time> [label]` where `<time>` is something like 5d 3h 2m 15s (which turns to 5 days, 2 hours, 2 minutes and 15 seconds). The label
is optional and you can leave it out or set it to tell you what a given timer is about.

## Development

### Setup
- Install Ruby (Tests run in 2.6.3)
- Set up dependencies:
  ```sh
  gem install bundler
  bundle install
  ```

### Running Tests

```
bundle exec rspec
```

### Building the package

```
bundle exec rake package
```
