require_relative '../lib/filter_timers.rb'
require_relative '../lib/timer.rb'

describe 'get_timers' do
  it 'returns an empty json items if nothing exists' do
    expect(JSON.parse(get_timers)).to eq('items' => [])
  end

  it 'returns a single item based on a file' do
    File.write('timer_1.txt', "#{format_timer(12_345)}, 30m timer")
    items = JSON.parse(get_timers)['items']
    expect(items).to have_attributes(length: 1)
    expect(items.first['title']).to eq('3:25:45 (30m timer)')
    expect(items.first['subtitle']).to eq('30m timer has 3:25:45 left, ⌘+⏎ to remove.')
    FileUtils.rm('timer_1.txt')
  end

  it 'returns 3 items' do
    File.write('timer_1.txt', "#{format_timer(123)}, 30h timer")
    File.write('timer_2.txt', "#{format_timer(1234)}, 30h timer")
    File.write('timer_3.txt', "#{format_timer(12_345)}, 30h timer")
    items = JSON.parse(get_timers)['items']
    expect(items).to have_attributes(length: 3)
    FileUtils.rm('timer_1.txt')
    FileUtils.rm('timer_2.txt')
    FileUtils.rm('timer_3.txt')
  end
end
