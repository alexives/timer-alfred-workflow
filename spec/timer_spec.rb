require_relative '../lib/timer.rb'

describe 'format_timer' do
  it 'returns a string from seconds with hours' do
    expect(format_timer(14_400)).to eq('4:00:00')
  end

  it 'returns a string from seconds with minutes' do
    expect(format_timer(3000)).to eq('50:00')
  end

  it 'returns a string from seconds with only seconds' do
    expect(format_timer(30)).to eq('30s')
  end

  it 'returns a string with hours, but 0 minutes' do
    expect(format_timer(14_430)).to eq('4:00:30')
  end
end

describe 'seconds_from_string' do
  it 'parses days alone' do
    expect(seconds_from_string('2d')).to eq([172_800, '2d timer'])
  end

  it 'parses hours alone' do
    expect(seconds_from_string('2h')).to eq([7200, '2h timer'])
  end

  it 'parses minutes alone' do
    expect(seconds_from_string('2m')).to eq([120, '2m timer'])
  end

  it 'parses seconds alone' do
    expect(seconds_from_string('2s')).to eq([2, '2s timer'])
  end

  it 'parses seconds without "s"' do
    expect(seconds_from_string('30')).to eq([30, '30 timer'])
  end

  it 'parses hours minutes and seconds' do
    expect(seconds_from_string('2d 2h 2m 30s')).to eq([180_150, '2d 2h 2m 30s timer'])
  end

  it 'extracts a name' do
    expect(seconds_from_string('2d 2h 2m 30s jim comes to town')).to eq([180_150, 'jim comes to town'])
  end
end

describe 'set_timer' do
  let!(:now) { Time.now }
  it 'runs a timer' do
    expect(Time).to receive(:now).and_return(now, now, now + 10, now + 10)
    expect(Sleeper).to receive(:wait)
    expect(set_timer('10s')).to eq('10s timer is done!')
  end
end
